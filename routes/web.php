<?php
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/**
 * Role
 */
Route::get('/route-1', 'RoleController@route1')->name('route-1');
Route::get('/route-2', 'RoleController@route2')->name('route-2');
Route::get('/route-3', 'RoleController@route3')->name('route-3');
