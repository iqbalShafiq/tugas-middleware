<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role']);
    }

    public function route1()
    {
        return "Berhasil Masuk Route-1";
    }

    public function route2()
    {
        return "Berhasil Masuk Route-2";
    }

    public function route3()
    {
        return "Berhasil Masuk Route-3";
    }
}
