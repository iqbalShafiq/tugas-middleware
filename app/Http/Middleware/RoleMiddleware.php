<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->isPermitted(2) && request()->route()->uri === "route-3") {
            return $next($request);
        } else if (Auth::user()->isPermitted(1) && request()->route()->uri === "route-2") {
            return $next($request);
        } else if (Auth::user()->isPermitted(0) && request()->route()->uri === "route-1") {
            return $next($request);
        }

        abort(403);
    }
}
